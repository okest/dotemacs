;; moe-theme.el stuff
;; (require 'moe-theme)
;; (require 'moe-theme-switcher) ; change colour based on time of day

;; (setq calendar-latitude -39)
;; (setq calendar-longitude 139)

;; (moe-light)

;; (setq moe-theme-set-color 'green)

;; Helm
(require 'helm-config)

;; Evil
(require 'evil)

;; powerline
;;(require 'powerline)

(evil-mode 1)
(evil-snipe-mode 1)
(powerline-evil-center-color-theme)
