;; Hooks! --------------------------------------------------------

;; Arduino
;;(autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)
;;(add-to-list 'auto-mode-alist '("\.ino$" . arduino-mode))

;; Paredit!
(autoload 'enable-paredit-mode "paredit"
  "Turn on pseudo-structural editing of Lisp code." t)

(add-hook 'emacs-lisp-mode-hook
          #'enable-paredit-mode)
(add-hook 'ielm-mode-hook
          #'enable-paredit-mode)
(add-hook 'lisp-mode-hook
          #'enable-paredit-mode)
(add-hook 'scheme-mode-hook
          #'enable-paredit-mode)
(add-hook 'racket-mode-hook
          #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook
          #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook
          #'enable-paredit-mode)
(add-hook 'geiser-repl-mode-hook
          #'enable-paredit-mode)

;; Rainbows!
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'rainbow-identifiers-mode)

;; Flyspell!
(add-hook 'tex-mode-hook #'flyspell-mode)

;; Company!
(add-hook 'after-init-hook 'global-company-mode)

;; Forth!
(add-hook 'forth-mode-hook
          (function
           (lambda ()
             ;; customize variables here:
             (setq forth-indent-level 4)
             (setq forth-minor-indent-level 2)
             (setq forth-hilight-level 3)
             ;; ...
             )))

;; Gforth mode
(autoload 'forth-mode "../gforth.el")
(autoload 'forth-block-mode "../gforth.el")
(setq auto-mode-alist
      (cons '("\\.fs\\'" . forth-mode)           
            auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.fb\\'" . forth-block-mode)
            auto-mode-alist))

