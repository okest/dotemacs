(require 'org-toodledo)
(setq org-toodledo-userid "td561b9b5a59128")
(setq org-toodledo-password "n5<S`69`DG@U")
(setq org-toodledo-file "/home/walter/projects/org/uni/2015/sem2.org")

(add-hook 'org-mode-hook
          (lambda ()
            (local-unset-key "\C-o")
            (local-set-key "\C-od" 'org-toodledo-mark-task-deleted)
            (local-set-key "\C-os" 'org-toodledo-sync)))
(add-hook 'org-agenda-mode-hook
          (lambda ()
            (local-unset-key "\C-o")
            (local-set-key "\C-od" 'org-toodledo-mark-task-deleted))
